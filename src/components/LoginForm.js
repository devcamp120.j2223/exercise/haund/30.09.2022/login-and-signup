import { useState } from "react";
import { Row, Col, Input, Button } from "reactstrap";

const LoginForm = () => {
    const [email, setEmail] = useState("");
    const [password, setPassWord] = useState("");

    const validateInput = () => {
        if(email === "") {
            alert("Vui lòng nhập email");
            return false;
        }
        if(password === "") {
            alert("Vui long nhập password");
            return false;
        }
        alert("Nội dung hợp lệ");
        console.log(`Email:  ${email}`);
        console.log(`Password: ${password}`);
    }

    return(
        <Row className="text-center">
            <Col className="col-12 mt-3">
                <h4 className="text-light">Welcome Back!</h4>
            </Col>
                <Col className="col-12 mt-3">
                    <Input placeholder="Email Address *" value={email} onChange={(event) => {setEmail(event.target.value)}} />
                </Col>
                <Col className="col-12 mt-3">
                    <Input placeholder="Password *" type="password" value={password} onChange={(event) => {setPassWord(event.target.value)}}/>
                </Col>
            <Col className="col-12 mt-3">
                <Button color="success" style={{width: "100%"}} onClick={validateInput}>LOG IN</Button>
            </Col>
        </Row>
    )
}

export default LoginForm;