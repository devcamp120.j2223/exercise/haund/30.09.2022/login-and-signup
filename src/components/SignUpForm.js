import { useState } from "react";
import { Row, Col, Input, Button } from "reactstrap";

const SignUpForm = () => {
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassWord] = useState("");

    const validateInput = () => {
        if(firstName === "") {
            alert("vui lòng nhập Frist name");
            return false;
        }
        if(lastName === "") {
            alert("vui lòng nhập Last name");
            return false;
        }
        if(email === "") {
            alert("Vui lòng nhập email");
            return false;
        }
        if(password === "") {
            alert("Vui long nhập password");
            return false;
        }
        alert("Nội dung hợp lệ");
        console.log(`First name: ${firstName}`);
        console.log(`Last name: ${lastName}`);
        console.log(`Email: ${email}`);
        console.log(`Password: ${password}`);

    }

    return(
        <div>
<Row className="text-center">
                <Col className="col-12 mt-3">
                    <h4 className="text-light">Sign Up For Free</h4>
                </Col>
                <Col className="col-12 mt-3">
                    <Row>
                        <Col className="col-6">
                            <Input placeholder="First name *" value={firstName} onChange={(event) => {setFirstName(event.target.value)}}/>
                        </Col>
                        <Col className="col-6">
                            <Input placeholder="Last name *" value={lastName} onChange={(event) => {setLastName(event.target.value)}}/>
                        </Col>
                    </Row>
                </Col>
                <Col className="col-12 mt-3">
                    <Input placeholder="Email Address *" value={email} onChange={(event) => {setEmail(event.target.value)}}/>
                </Col>
                <Col className="col-12 mt-3">
                    <Input placeholder="Password *" type="password" value={password} onChange={(event) => {setPassWord(event.target.value)}}/>
                </Col>
                <Col className="col-12 mt-3">
                    <Button color="success" style={{ width: "100%" }} onClick={validateInput}>LOG IN</Button>
                </Col>
            </Row>
        </div>
    )
}

export default SignUpForm;