import "bootstrap/dist/css/bootstrap.min.css"
import { useState } from "react";
import { ButtonGroup, Button } from "reactstrap";

import LoginForm from "./components/LoginForm";
import SignUpForm from "./components/SignUpForm";

function App() {
  const [isLogIn, setIsLogIn] = useState(true);

  const logInClick = () => {
    setIsLogIn(true);
  }

  const signUpClick = () => {
    setIsLogIn(false);
  }

  return (
    <div className="container" style={{height: "100vh", display: "flex", flexDirection: "column", justifyContent: "center", backgroundColor: '#333'}}>
      <div className="row" style={{justifyContent: "center"}}>
        <div className="col col-6">
          <ButtonGroup style={{width: "100%"}}>
            <Button color={isLogIn ? "success" : "secondary"} onClick={logInClick} >Log In</Button>
            <Button color={!isLogIn ? "success" : "secondary"} onClick={signUpClick} >Sign Up</Button>
          </ButtonGroup>

          {
            isLogIn ?
              <LoginForm />
              :
              <SignUpForm /> 
          }
      </div>
    </div>
  </div>
  );
}

export default App;
